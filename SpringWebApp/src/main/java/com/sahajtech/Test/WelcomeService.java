package com.sahajtech.Test;

import org.springframework.boot.test.autoconfigure.properties.PropertyMapping;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
@PropertyMapping("/service")
public class WelcomeService {
	
	@PropertyMapping("/sayHello")
	public String sayHello(){
		return "Hello Welcome SpringBoot Hello Service !!!";
	}
	/*@RequestMapping(value="/list")
	public String testing(){
		return "Hello from Bhavik Working on spring book";
		
	}*/
}
