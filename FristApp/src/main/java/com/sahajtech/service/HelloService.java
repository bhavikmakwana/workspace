package com.sahajtech.service;

import org.springframework.stereotype.Service;

@Service("helloService")
public class HelloService {
	

	public String sayHello(){
		return "Say Hello Service";
	}
}
