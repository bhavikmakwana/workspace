package com.sahajtech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.sahajtech.service.HelloService;

@SpringBootApplication
public class FristAppApplication {

	public static void main(String[] args) {
		ApplicationContext context =
				SpringApplication.run(FristAppApplication.class, args);
		HelloService service = context.getBean("helloService",HelloService.class);
		System.out.println(service.sayHello());
	}
}
