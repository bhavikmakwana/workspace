<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
	<form action="LoginServlet" method="post">
		<div>
			<table>
				<th><td>${error}</td></th>
				<tr>
					<td><lable>User Id</lable>:</td>
					<td><input type="text" name="username" id="username"><br></td>
				</tr>
				<tr>
					<td><lable>Password</lable>:</td>
					<td><input type="password" id="password" name="password"></td>
				</tr>
				<tr>
					<td><input type="submit" id="Login"></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>