package com.sahajtech.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(asyncSupported = true, description = "Login Servlet", urlPatterns = { "/LoginServlet" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("name");
		System.out.println("Your name is : " + name);
		request.setAttribute("name", name);
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher("/WEB-INF/JSP/Welcome.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("<------------------------inside login servlet------------------------> ");
		// TODO Auto-generated method stub
		String name = request.getParameter("username");
		System.out.println("name -> "+name);
		String password = request.getParameter("password");
		System.out.println("password  -> "+password);
		
		request.setAttribute("name", name);
		request.setAttribute("password", password);
		if((name.isEmpty() || name.equals(null)) && (password.isEmpty() || password.equals(null)) ){
			
			request.setAttribute("error", "Please Enter user id and password for Login");	
			request.getRequestDispatcher("/WEB-INF/JSP/Login.jsp").forward(request, response);
		}else{
			if(name.equals(password)){
				request.setAttribute("usernmae", name);
				request.getRequestDispatcher("/WEB-INF/JSP/Welcome.jsp").forward(request, response);
			}else{
				request.setAttribute("error", "invalid login id and password");	
				request.getRequestDispatcher("/WEB-INF/JSP/Login.jsp").forward(request, response);
			}	
		}
	}

}
